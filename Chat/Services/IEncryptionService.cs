﻿using Messenger.CoreModels;

namespace Messenger.Services
{
    public interface IEncryptionService
    {
        void Encrypt(Message message, int key);

        Message Decrypt(Message message, int key);
    }
}
