﻿using Messenger.Contexts;
using Messenger.CoreModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Messenger.Controllers
{
    public class UserController : Controller
    {
        private readonly MessengerDbContext dbContext;

        public UserController(MessengerDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var result = new List<User>();
            if (User.Identity.IsAuthenticated)
            {
                var users = dbContext.Users.ToList();
                var user = dbContext.Users.FirstOrDefault(user => user.Email == User.Identity.Name);
                var userFriends = dbContext.UserFriends
                    .Where(x => (x.UserId == user.Id || x.FriendId == user.Id))
                    .ToList();

                for (int j = 0; j < users.Count; j++)
                {
                    if (user.Id == users[j].Id)
                    {
                        continue;
                    }

                    if (!userFriends.Any(x => x.FriendId == users[j].Id || x.UserId == users[j].Id) 
                        && users[j].IsVisible)
                    {
                        result.Add(users[j]);
                    }
                }
            }

            return View(result);
        }

        [HttpPost]
        public IActionResult Index(User user)
        {
            return View(dbContext.Users.FirstOrDefault(x => x.UId == user.UId));
        }
    }
}
