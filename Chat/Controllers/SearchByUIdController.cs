﻿using Messenger.Contexts;
using Messenger.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Messenger.Controllers
{
    public class SearchByUIdController : Controller
    {
        private readonly MessengerDbContext dbContext;

        public SearchByUIdController(MessengerDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(SearchByUIdViewModel model)
        {
            var user = dbContext.Users.FirstOrDefault(x => x.UId == model.UId);

            if(user is null)
            {
                return NotFound();
            }

            return RedirectToAction("Invite", "Invitation", new { id = user.Id });
        }
    }
}
