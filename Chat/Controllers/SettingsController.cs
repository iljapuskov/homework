﻿using Messenger.Contexts;
using Messenger.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Messenger.Controllers
{
    public class SettingsController : Controller
    {
        private readonly MessengerDbContext dbContext;

        public SettingsController(MessengerDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        [HttpGet]
        public IActionResult Index() 
        {
            var user = dbContext.Users.FirstOrDefault(x => x.Email == User.Identity.Name);

            return View(new SettingsViewModel
            {
                UId = user.UId,
                IsVisible = user.IsVisible
            });
        }

        [HttpPost]
        public IActionResult Index(SettingsViewModel model)
        {
            if (model.NewUserName != null)
            {
                dbContext.Users.FirstOrDefault(x => x.Email == User.Identity.Name).UserName = model.NewUserName;
            }

            if (model.NewPassword != null)
            {
                dbContext.Users.FirstOrDefault(x => x.Email == User.Identity.Name).Password = model.NewPassword;
            }

            if(model.IsVisible != dbContext.Users.FirstOrDefault(x => x.Email == User.Identity.Name).IsVisible)
            {
                dbContext.Users.FirstOrDefault(x => x.Email == User.Identity.Name).IsVisible = model.IsVisible;
            }

            dbContext.SaveChanges();

            return RedirectToAction("Index", "Home");
        }
    }
}
