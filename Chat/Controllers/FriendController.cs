﻿using Messenger.Contexts;
using Messenger.CoreModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Messenger.Controllers
{
    [Authorize]
    public class FriendController : Controller
    {
        private readonly MessengerDbContext dbContext;

        public FriendController(MessengerDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public IActionResult Index()
        {
            var result = new List<User>();
            if (User.Identity.IsAuthenticated)
            {
                var users = dbContext.Users.ToList();
                var user = dbContext.Users.FirstOrDefault(user => user.Email == User.Identity.Name);
                var userFriends = dbContext.UserFriends
                    .Where(x => (x.UserId == user.Id || x.FriendId == user.Id) && x.IsVerified)
                    .ToList();

                for(int i = 0; i < userFriends.Count; i++)
                {
                    for(int j = 0; j < users.Count; j++)
                    {
                        if(user.Id == users[j].Id)
                        {
                            continue;
                        }

                        if(users[j].Id == userFriends[i].FriendId || users[j].Id == userFriends[i].UserId)
                        {
                            result.Add(users[j]);
                        }
                    }
                }
            }

            return View(result);
        }

        [HttpGet]
        public IActionResult Delete(int? id)
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = dbContext.Users.FirstOrDefault(user => user.Email == User.Identity.Name);

                var userFriend = dbContext.UserFriends
                    .FirstOrDefault(x => (x.FriendId == id || x.UserId == id) && (x.FriendId == user.Id || x.UserId == user.Id));

                if (userFriend is null)
                {
                    return BadRequest();
                }

                dbContext.UserFriends.Remove(userFriend);
                dbContext.SaveChanges();
            }

            return RedirectToAction("Index", "Friend");
        }
    }
}
