﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Messenger.Models
{
    public class SearchByUIdViewModel
    {
        public int UId { get; set; }
    }
}
