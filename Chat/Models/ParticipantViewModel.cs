﻿namespace Messenger.Models
{
    public class ParticipantViewModel
    {
        public int Id { get; set; }

        public string UserName { get; set; }
    }
}
