﻿namespace Messenger.Models
{
    public class EditMessageViewModel
    {
        public int Id { get; set; }

        public string Message { get; set; }
    }
}
