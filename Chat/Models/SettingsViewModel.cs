﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Messenger.Models
{
    public class SettingsViewModel
    {
        public int UId { get; set; }

        public string NewUserName { get; set; }

        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        public bool IsVisible { get; set; }
    }
}
