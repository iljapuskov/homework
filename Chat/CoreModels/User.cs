﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Messenger.CoreModels
{
    public class User
    {
        public int Id { get; set; }

        public int UId { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public bool IsVisible { get; set; }

        public int Key { get; set; }
    }
}
